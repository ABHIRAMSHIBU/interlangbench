#include<stdio.h>
#include<sys/time.h>
#include<math.h>
long long get_time(){
    struct timeval t;
    gettimeofday( &t,NULL );
    long long time_in_usec = t.tv_sec * pow(10,6) + t.tv_usec;
    return time_in_usec;
}