#!/usr/bin/env sh
echo "Compiling.."
gcc genfloat.c -o genfloat
javac bench.java
echo "Done Compiling..."
echo "Generating Dataset.."
./genfloat
echo "Done Generating Dataset..."
echo "Benchmarking..."
java bench
echo "Done Benchmarking"