#include<stdio.h>
#include<stdlib.h>
#include "time.c"
int main(){
    FILE * f = fopen("/tmp/values","w");
    double a[1000];
    srand(100);
    for(int i=0;i<1000;i++){
        a[i]=rand()/1000.0;
    }
    long long start=get_time(),end;
    for(int j=0;j<100000;j++){
        for(int i=0;i<1000;i++){
            fprintf(f,"%f\n",a[i]);
        }
    }
    end=get_time();
    fclose(f);
    printf("C: File write benchmark Result\n");
    printf("100000 File operations, Average took: %f micoseconds\n",(((float)(end-start))/100000));
    return 0;
}