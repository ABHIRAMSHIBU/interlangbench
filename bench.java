import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;
public class bench{
    public static void main(String args[]) throws FileNotFoundException{
        File f = new File("/tmp/values");
        float a[] =new float[1000],temp;
        Scanner s = new Scanner(f);
        long start =System.nanoTime()/1000,end;
        for(int j=0;j<1000;j++){
            for(int i=0;i<1000;i++){
                a[i]=s.nextFloat();
            }
        }
        end = System.nanoTime()/1000;
        System.out.println("Java: File read benchmark Result");
        System.out.println("1000 File operations, Average took: "+((end-start)/1000.0)+" micoseconds");
        s.close();
        start=System.nanoTime()/1000;
        for(int k=0;k<10;k++){
            for(int i=0;i<1000;i++){
                for(int j=0;j<1000;j++){
                    temp=(j*i)-i;
                }
            }
        }
        end = System.nanoTime()/1000;
        System.out.println("Java: Floatbenchmark Result");
        System.out.println("Float operations, Average took: "+((end-start)/100000.0)+" micoseconds");
    }
}